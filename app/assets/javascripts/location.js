function success(position) {
  var latitude = position['coords']['latitude']
  var longitude = position['coords']['longitude']
  $.ajax({
    url: 'locations/',
    data: {latitude: latitude, longitude: longitude}
  }).done(function(response){
    $('.loading').hide();
    $('.results').append(response);
  })
  
}

function error(msg) {
  console.log('error');
  // var s = document.querySelector('#status');
  // s.innerHTML = typeof msg == 'string' ? msg : "failed";
  // s.className = 'fail';
  
  // console.log(arguments);
}

if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(success, error);
} else {
  error('not supported');
}
