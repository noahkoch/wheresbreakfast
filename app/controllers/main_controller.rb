class MainController < ApplicationController
  def index
    client = Weatherman::Client.new
    @current_temp = client.lookup_by_location('Toronto').condition['temp']
  end
end
