class LocationsController < ApplicationController
  def index
    latitude = params[:latitude]
    longitude = params[:longitude]
    coordinates = { latitude: latitude, longitude: longitude }
    render partial: 'locations_list_view', locals: {results: Yelp.client.search_by_coordinates(coordinates, { term: 'breakfast' })}
  end

end
